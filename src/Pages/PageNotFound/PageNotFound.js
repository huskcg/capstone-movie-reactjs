import React from "react";
import Lottie from "lottie-react";
import page_not_found from "../../Assets/404-website-error-animation.json";
export default function PageNotFound() {
  return (
    <div className="container">
      <Lottie animationData={page_not_found}></Lottie>
    </div>
  );
}
