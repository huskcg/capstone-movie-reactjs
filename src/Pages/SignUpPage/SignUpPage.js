import React from "react";
import { Form, Input, Button, Select, message, Typography } from "antd";
import { userServ } from "../../Services/userService";
import { useNavigate } from "react-router-dom";
import animation_login from "../../Assets/login-animate.json";
import Lottie from "lottie-react";

const SignUpPage = () => {
  let navigate = useNavigate();
  const onFinish = (values) => {
    userServ
      .signup(values)
      .then((result) => {
        console.log(result);
        message.success("Đăng ký thành công");
        navigate("/login");
      })
      .catch((err) => {
        message.error("Đăng ký thất bại. " + err.response.data.content);
        console.log(err.response.data.content);
      });
    console.log("Received values of form: ", values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const handleChange = (value) => {
    console.log(`selected ${value}`);
  };

  return (
    <div className="w-screen h-screen p-20 flex justify-center items-center">
      <div className="container p-20 bg-white rounded-lg flex">
        <div className="w-1/2 h-full">
          <Lottie animationData={animation_login} loop={true} />
        </div>
        <div className="w-1/2 h-full">
          <Typography.Title
            level={2}
            style={{ marginBottom: "2rem", textAlign: "center" }}
          >
            Đăng ký
          </Typography.Title>
          <Form
            name="basic"
            labelCol={{
              span: 0,
            }}
            wrapperCol={{
              span: 24,
            }}
            className="w-full"
            initialValues={{ remember: true, maNhom: "GP01" }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            layout="vertical"
          >
            <Form.Item
              label="Tài khoản"
              name="taiKhoan"
              rules={[
                { required: true, message: "Vui lòng nhập tài khoản!" },
                { min: 4, message: "Tài khoản phải có ít nhất 4 ký tự!" },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Mật khẩu"
              name="matKhau"
              rules={[
                { required: true, message: "Vui lòng nhập mật khẩu!" },
                { min: 6, message: "Mật khẩu phải có ít nhất 6 ký tự!" },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              name={"hoTen"}
              label="Họ tên"
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              name={"email"}
              label="Email"
              rules={[
                {
                  required: true,
                  type: "email",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              name="soDt"
              label="Số điện thoại"
              rules={[
                { required: true, message: "Please input your phone number!" },
              ]}
            >
              <Input style={{ width: "100%" }} />
            </Form.Item>

            <Form.Item
              name="maNhom"
              label="Mã nhóm"
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <Select
                style={{ width: 120 }}
                onChange={handleChange}
                options={[
                  { value: "GP01", label: "GP01" },
                  { value: "GP02", label: "GP02" },
                  { value: "GP03", label: "GP03" },
                  { value: "GP04", label: "GP04" },
                  { value: "GP05", label: "GP05" },
                  { value: "GP06", label: "GP06" },
                  { value: "GP07", label: "GP07" },
                  { value: "GP08", label: "GP08" },
                  { value: "GP09", label: "GP09" },
                  { value: "GP10", label: "GP10" },
                ]}
              />
            </Form.Item>

            <Form.Item>
              <Button danger type="primary" htmlType="submit">
                Đăng ký
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
};

export default SignUpPage;
