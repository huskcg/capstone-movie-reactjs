import React, { useEffect, useState } from "react";
import { movieServ } from "../../../Services/movieService";
import { Tabs } from "antd";
import ItemTabsMovie from "./ItemTabsMovie";

const onChange = (key) => {
  console.log(key);
};
// const items = [
//   {
//     key: "1",
//     label: `Tab 1`,
//     children: `Content of Tab Pane 1`,
//   },
// ];

export default function MovieTab() {
  const [danhSachHeThongRap, setDanhSachHeThongRap] = useState([]);

  useEffect(() => {
    movieServ
      .getMovieByTheater()
      .then((res) => {
        console.log(res.data.content);
        setDanhSachHeThongRap(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  let renderHeThongRap = () => {
    return danhSachHeThongRap.map((heThongRap) => {
      return {
        key: heThongRap.maHeThongRap,
        label: <img className="h-16" src={heThongRap.logo} alt="" />,
        children: (
          <Tabs
            style={{ height: 700 }}
            tabPosition="left"
            defaultActiveKey="1"
            items={heThongRap.lstCumRap.map((cumRap) => {
              return {
                key: cumRap.tenCumRap,
                label: (
                  <div className="w-60 truncate ">
                    <p className="text-cumrap font-medium">
                      {cumRap.tenCumRap}
                    </p>
                    <p className="text-xs text-gray-300">{cumRap.diaChi}</p>
                  </div>
                ),
                children: (
                  <div
                    className="space-y-5"
                    style={{ height: 700, overflowY: "scroll" }}
                  >
                    {cumRap.danhSachPhim.map((phim, index) => {
                      return <ItemTabsMovie phim={phim} key={index} />;
                    })}
                  </div>
                ),
              };
            })}
            onChange={onChange}
          />
        ),
      };
    });
  };
  return (
    <div className="container">
      <Tabs
        style={{ height: 700 }}
        tabPosition="left"
        defaultActiveKey="1"
        items={renderHeThongRap()}
        onChange={onChange}
      />
    </div>
  );
}
