import moment from "moment/moment";
import React from "react";

export default function ItemTabsMovie({ phim }) {
  //   console.log("phim :", phim);
  return (
    <div className="flex items-center space-x-5 border-b border-black pb-3">
      <img
        src={phim.hinhAnh}
        className="w-28 h-48 object-cover object-top rounded"
        alt=""
      />
      <div>
        <h5 className="font-medium text-xl mb-5 text-white">{phim.tenPhim}</h5>
        <div className="grid gap-5 grid-cols-3">
          {phim.lstLichChieuTheoPhim.slice(0, 6).map((item, index) => {
            return (
              <span
                key={index}
                className="rounded p-3 text-white bg-red-500 font-medium"
              >
                {moment(item.ngayChieuGioChieu).format("dd-mm-yyyy ~ hh-mm")}
              </span>
            );
          })}
        </div>
      </div>
    </div>
  );
}
