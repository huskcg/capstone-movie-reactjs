import React from "react";
import { Button, Card } from "antd";
import { NavLink } from "react-router-dom";
const { Meta } = Card;

export default function ItemMovie({ data }) {
  // console.log(data);
  return (
    <Card
      hoverable
      style={{
        width: "100%",
      }}
      cover={
        <img
          className="h-72 object-cover object-top"
          alt="example"
          src={data.hinhAnh}
        />
      }
    >
      <div
        style={{
          textAlign: "center",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          gap: "1.5rem",
        }}
      >
        <Meta title={data.tenPhim} style={{ width: "100%" }} />
        <NavLink to={`/detail/${data.maPhim}`}>
          <Button danger type="primary" className="text-center">
            Xem ngay
          </Button>
        </NavLink>
      </div>
    </Card>
  );
}
