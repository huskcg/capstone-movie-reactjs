import React from "react";
import MovieList from "./MovieList/MovieList";
import MovieTab from "./MovieTabs/MovieTab";

export default function HomePage() {
  return (
    <div className="space-y-20">
      <MovieList />
      <MovieTab />
    </div>
  );
}
