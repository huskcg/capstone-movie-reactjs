import { Button, Form, Input, message, Typography } from "antd";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { SET_USER_LOGIN } from "../../Redux/constant/userConstant";
import { localUserServ } from "../../Services/localStorage";
import { userServ } from "../../Services/userService";
import Lottie from "lottie-react";
import animation_login from "../../Assets/login-animate.json";

const LoginPage = () => {
  let navigate = useNavigate();
  let dispatch = useDispatch();
  const onFinish = (values) => {
    userServ
      .login(values)
      .then((res) => {
        message.success("Đăng nhập thành công");
        // Đưa thông tin lên redux
        dispatch({
          type: SET_USER_LOGIN,
          payload: res.data.content,
        });
        // Lưu thông tin vào local storage
        localUserServ.set(res.data.content);
        // Chuyển hướng user về home sau khi đăng nhập thành công
        navigate("/");
        console.log(res.data.content);
      })
      .catch((err) => {
        message.error("Đăng nhập thất bại");
        console.log(err);
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div className="w-screen h-screen p-20 flex justify-center items-center">
      <div className="container p-20 bg-white rounded-lg flex">
        <div className="w-1/2 h-full">
          <Lottie animationData={animation_login} loop={true} />
        </div>
        <div className="w-1/2 h-full">
          <Typography.Title
            level={2}
            style={{ marginBottom: "2rem", textAlign: "center" }}
          >
            Đăng nhập
          </Typography.Title>
          <Form
            name="basic"
            labelCol={{
              span: 0,
            }}
            wrapperCol={{
              span: 24,
            }}
            className="w-full"
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            layout="vertical"
          >
            <Form.Item
              label="Tài khoản"
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Please input your username!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Mật khẩu"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              style={{ textAlign: "center" }}
              wrapperCol={{
                offset: 0,
                span: 24,
              }}
            >
              <Button
                danger
                type="primary"
                htmlType="submit"
                style={{ marginTop: "2rem" }}
              >
                Đăng nhập
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
};
export default LoginPage;
