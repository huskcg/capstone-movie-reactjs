import React, { useEffect, useState } from "react";
import { NavLink, useParams } from "react-router-dom";
import { movieServ } from "../../Services/movieService";
import { Progress } from "antd";

export default function DetailPage() {
  let param = useParams();
  // console.log("param :", param);

  const [movie, setMovie] = useState({});
  useEffect(() => {
    //call api get detail by ID
    // movieServ
    //   .getDetailMovie(param.id)
    //   .then((res) => {
    //     console.log(res);
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //   });

    // Su dung async await
    let fetchDetail = async () => {
      try {
        let res = await movieServ.getDetailMovie(param.id);
        console.log("res", res);
        // set state neu co data
        setMovie(res.data.content);
      } catch (error) {
        console.log("error :", error);
      }
    };
    fetchDetail();
  }, [param.id]);

  return (
    <div className="container flex">
      <img className="w-1/4" src={movie.hinhAnh} alt="" />
      <div className="p-5">
        <h2 className="text-xl font-medium text-white mb-5">{movie.tenPhim}</h2>
        <p className="text-sm text-gray-600 mb-5">{movie.moTa}</p>
        <Progress
          className="mr-5"
          strokeColor={{
            "0%": "#009FFF",
            "100%": "#ec2F4B",
          }}
          format={(percent) => {
            return `${percent / 10}/10 Point`;
          }}
          size={100}
          type="circle"
          percent={movie.danhGia * 10}
        />
        <NavLink
          to={`/booking/${movie.maPhim}`}
          className="px-5 py-2 rounded bg-red-500 text-white"
        >
          Mua vé
        </NavLink>
      </div>
    </div>
  );
}
