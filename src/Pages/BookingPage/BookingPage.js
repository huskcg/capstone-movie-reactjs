import React from "react";
import Lottie from "lottie-react";
import under_construction from "../../Assets/website-under-construction-animate.json";

export default function BookingPage() {
  return (
    <div className="container">
      <Lottie animationData={under_construction}></Lottie>
    </div>
  );
}
