import React from "react";
import UserMenu from "./UserMenu";
import { NavLink } from "react-router-dom";

export default function HeaderMobile() {
  return (
    <div className="h-20 shadow" style={{ backgroundColor: "#06121e" }}>
      <div className="container mx-auto px-5 flex flex-col md:flex-row items-center justify-between w-full">
        <NavLink to={"/"}>
          <span className="font-bold text-red-600 text-2xl">
            Capstone Movie
          </span>
        </NavLink>
        <UserMenu />
      </div>
    </div>
  );
}
