import React from "react";
import { Desktop, Mobile, Tablet } from "../../Layout/Responsive";
import HeaderDesktop from "./HeaderDesktop";
import HeaderTablet from "./HeaderTablet";
import HeaderMobile from "./HeaderMobile";

export default function Header() {
  return (
    <div>
      <Desktop>
        <HeaderDesktop></HeaderDesktop>
      </Desktop>
      <Tablet>
        <HeaderTablet></HeaderTablet>
      </Tablet>
      <Mobile>
        <HeaderMobile></HeaderMobile>
      </Mobile>
    </div>
  );
}
