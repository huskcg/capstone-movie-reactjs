import React from "react";
import { NavLink } from "react-router-dom";
import UserMenu from "./UserMenu";

export default function HeaderTablet() {
  return (
    <div className="h-20 shadow" style={{ backgroundColor: "#06121e" }}>
      <div className="container mx-auto px-5 flex justify-between items-center h-full">
        <NavLink to={"/"}>
          <span className="font-bold text-red-600 text-3xl">
            Capstone Movie
          </span>
        </NavLink>
        <UserMenu />
      </div>
    </div>
  );
}
