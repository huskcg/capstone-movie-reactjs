import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { localUserServ } from "../../Services/localStorage";

export default function UserMenu() {
  let userInfo = useSelector((state) => {
    return state.userReducer.userInfo;
  });
  // console.log(typeof userInfo);
  //TH1: null => false
  //TH2: {} => true
  let handleLogout = () => {
    localUserServ.remove();
    // window.location.href = "/login";
    window.location.reload();
  };
  let renderContent = () => {
    // console.log(userInfo);
    if (userInfo) {
      return (
        <div className="space-x-5">
          <span className="font-bold text-white">{userInfo.hoTen}</span>
          <button
            onClick={handleLogout}
            className="my-button px-5 py-2 rounded border-black border-2"
            style={{ backgroundColor: "white" }}
          >
            Đăng xuất
          </button>
        </div>
      );
    } else {
      return (
        <div className="space-x-5">
          <NavLink to={"/login"}>
            <button
              className="my-button px-5 py-2 rounded border-black border-2"
              style={{ backgroundColor: "white" }}
            >
              Đăng nhập
            </button>
          </NavLink>
          <NavLink to={"/signup"}>
            <button
              className="my-button px-5 py-2 rounded border-black border-2"
              style={{ backgroundColor: "white" }}
            >
              Đăng ký
            </button>
          </NavLink>
        </div>
      );
    }
  };
  return <div>{renderContent()}</div>;
}
