import React from "react";
import UserMenu from "./UserMenu";
import { NavLink } from "react-router-dom";

export default function HeaderDesktop() {
  return (
    <div className="h-20 shadow" style={{ backgroundColor: "#06121e" }}>
      <div className="container mx-auto px-5 flex justify-between items-center h-full">
        <NavLink to={"/"}>
          <span className="font-bold text-red-600 text-4xl">
            Capstone Movie
          </span>
        </NavLink>
        <UserMenu />
      </div>
    </div>
  );
}
