import React from "react";
import { AiFillFacebook, AiFillMail, AiFillPhone } from "react-icons/ai";

export default function Footer() {
  return (
    <div className="footer bg-[url('https://phimbag.com/static/skin/footer-bg.jpg')]">
      <div className="container p-5">
        <p className="font-bold text-lg text-white">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Sed, quasi
          eaque
        </p>
        <ul className="mt-3 p-5 list-disc">
          <li className="text-gray-400">Lorem ipsum dolor sit amet.</li>
          <li className="text-gray-400">Lorem ipsum dolor sit amet.</li>
          <li className="text-gray-400">Lorem ipsum dolor sit amet.</li>
          <li className="text-gray-400">Lorem ipsum dolor sit amet.</li>
          <li className="text-gray-400">Lorem ipsum dolor sit amet.</li>
          <li className="text-gray-400">Lorem ipsum dolor sit amet.</li>
        </ul>
        <div className="flex space-x-3">
          <a href="#!">
            <AiFillFacebook className="text-4xl text-yellow-100 hover:bg-slate-400 rounded-full" />
          </a>
          <a href="#!">
            <AiFillMail className="text-4xl text-yellow-100 hover:bg-slate-400 rounded-full" />
          </a>
          <a href="#!">
            <AiFillPhone className="text-4xl text-yellow-100 hover:bg-slate-400 rounded-full" />
          </a>
        </div>
      </div>
    </div>
  );
}
