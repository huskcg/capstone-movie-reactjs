import React from "react";
import Header from "../Components/Header/Header";
import Footer from "../Components/Footer/Footer";

export default function Layout({ Component }) {
  return (
    <div className="h-full min-h-screen flex flex-col">
      <Header />
      <div className="flex-grow py-10" style={{ backgroundColor: "#06121e" }}>
        <Component />
      </div>
      <Footer />
    </div>
  );
}
