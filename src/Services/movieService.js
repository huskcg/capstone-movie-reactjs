// import axios from "axios";
// import { BASE_URL, configHeaders } from "./config";
import { https } from "./config";

export const movieServ = {
  getMovieList: () => {
    //C1: Su dung axios binh thuong
    // return axios({
    //   url: `${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP04`,
    //   method: "GET",
    //   headers: configHeaders(),

    // });

    //C2: Su dung axios instance
    return https.get("/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP03");
  },
  getMovieByTheater: () => {
    //C1: Su dung axios binh thuong
    // return axios({
    //   url: `${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuHeThongRap`,
    //   method: "GET",
    //   headers: configHeaders(),
    // });

    //C2: Su dung axios instance
    return https.get("/api/QuanLyRap/LayThongTinLichChieuHeThongRap");
  },
  getDetailMovie: (id) => {
    return https.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`);
  },
};
