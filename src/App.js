import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import HomePage from "./Pages/HomePage/HomePage";
import LoginPage from "./Pages/LoginPage/LoginPage";
import DetailPage from "./Pages/DetailPage/DetailPage";
import Layout from "./Layout/Layout";
import BookingPage from "./Pages/BookingPage/BookingPage";
import PageNotFound from "./Pages/PageNotFound/PageNotFound";
import SignUpPage from "./Pages/SignUpPage/SignUpPage";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="" element={<Layout Component={HomePage} />} />
          <Route path="/login" element={<Layout Component={LoginPage} />} />
          <Route
            path="/detail/:id"
            element={<Layout Component={DetailPage} />}
          />
          <Route
            path="/booking/:id"
            element={<Layout Component={BookingPage} />}
          />
          <Route path="*" element={<Layout Component={PageNotFound} />} />
          <Route path="/signup" element={<Layout Component={SignUpPage} />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
